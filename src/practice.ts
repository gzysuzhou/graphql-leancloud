import * as AV from 'leancloud-storage'


async function exec1() {
    const query = new AV.Query('Students');
    const objs = await query.find() as AV.Object[];
    // for(var i =0; i< objs.length;i++){
    //     console.log(objs[i].get('name'));
    // }
    objs.map(it=>{
        console.log(it.get('name'));
    });
}

async function exec2(){
    const query = new AV.Query('Students');
    const obj = await query.get('5b9639819f545400398343f1') as AV.Object;
    console.log(obj);
    obj.set('age',23);
    await obj.save();
}

async function exec3(){
    const query = new AV.Query('Students');
    const obj = await query.equalTo('objectId','5b9639819f545400398343f1');
    console.log(obj);
}

//relations
async function exec4(){
    const query = new AV.Query('School');
    //query.include('Students');
    const objs = await query.find() as AV.Object[];
    for(var i=0; i< objs.length;i++){
       // console.log(objs[i].toJSON());
        console.log(objs[i].get('students'));
    }
}

async function exec5() {
    var Todo = AV.Object.extend('Todo');
    var todo = new Todo();
    todo.set('title','工程师周会');
    todo.set('content','每周例会内容');
    todo.set('location','会议室');
    const res = await todo.save();
    console.log(res.id);
}

async function exec6() {
    let TestObject = AV.Object.extend('DataTypeTest');
    let number = 1024
    let string = 'famous '+ number;
    let date = new Date();
    let array = [string, number];
    let obj = {number:number, string:string};
    let testObject = new TestObject();
    testObject.set('testNumber', number);
    testObject.set('testString', string);
    testObject.set('testDate', date);
    testObject.set('testArray', array);
    testObject.set('testObject', obj);
    const res = await testObject.save();
    console.log(res);

}

async function exec7() {
    let query = new AV.Query('Todo');
    const obj = await query.get('5b97281b808ca43cd2f66a97');
    console.log(obj);
}

async function exec8() {// ?
    let obj = AV.Object.createWithoutData('Todo','5b97281b808ca43cd2f66a97');
    const res = await obj.fetch();
    console.log(res);
}

async function exec9() {
    let Todo =  AV.Object.extend('Todo');
    let todo = new Todo();
        todo.set('title','get id');
        todo.set('content', 'get id');
    const obj = await todo.save();
    console.log(obj.id);
}

async function exec10() {
    let query = new AV.Query('Todo');
    try {
       const obj = await query.get('5b972dce1579a3003a23b21d');
       //console.log(obj.get('content'));
       console.log(obj.toJSON());
    } catch(err) {
        console.log(err);
    }
}

async function exec11() {
    let query = new AV.Query('Students');
    query.greaterThanOrEqualTo('age',30);
    const res = await query.find();
    res.forEach(item=>{
        console.log(item.toJSON());
    })
}

async function exec12() {
    let todo = AV.Object.createWithoutData('Todo','5b972782808ca43cd2f663ad');
    todo.set('view', 0);
    try{
        const obj = await todo.save();
        console.log(obj.get('view'));
        obj.increment('view', 1);
        //obj.fetchWhenSave(true);
        const obj2 = await obj.save();
        console.log(obj2.get('view'));
    }catch(err){
        console.log(err);
    }
}

//删除对象
async function exec13() {
    let obj = AV.Object.createWithoutData('Todo', '5b90ebf8ee920a003be0fd13');
    const res = await obj.destroy();
    console.log(res.toJSON());
}

//批量更新对象
async function exec14() {
    let query = new AV.Query('Todo');
    const todos = await query.find() as AV.Object[];
    todos.forEach(item=>{item.set('status', 1)});
    const res = await AV.Object.saveAll(todos);
    console.log(res);
}

// AND OR 逻辑查询
async function exec15() {
    let ageQuery = new AV.Query('Students');
    ageQuery.greaterThan('age', 40);
    let sexQuery = new AV.Query('Students');
    sexQuery.equalTo('sex', '0');
    let query =  AV.Query.or(ageQuery, sexQuery);
    const res = await query.find() as AV.Object[];
    res.forEach(element => {
        console.log(element.toJSON());
    });
}

//分页 skip limit
async function exec16() {
    let query = new AV.Query('Students');
    query.skip(1);
    query.limit(2);
    const res = await query.find();
    res.forEach(ele => {
        console.log(ele.toJSON());
    });
}

//字符串查询
async function exec17() {
    let query = new AV.Query('Students');
    //query.startsWith('name','su');
    //query.contains('name','su');
    //查询name 不包含suzhou的记录
    let regEXP= new RegExp(/^((?!suzhou).)*$/g);
    query.matches('name', regEXP);
    const res = await query.find();
    res.forEach(ele => {
        console.log(ele.toJSON());
    });
}

//关联查询
async function exec18() {//？
    let obj = AV.Object.createWithoutData('School','5b964b75570c350063fcd9b9');
    //console.log(obj);
    let relations = obj.relation('students');
    let query = relations.query();
    const res = await query.find();
    res.forEach(ele => {
        console.log(ele.toJSON());
    });
}

//关联查询2
async function exec19() {
    let obj = AV.Object.createWithoutData('TodoFolder', '5b9767789f545400398f1719');
    //console.log(obj.toJSON());
    let relation = obj.relation('tags');
    let query = relation.query();
    const res = await query.find();
    res.forEach(ele => {
        console.log(ele.toJSON());
    });
}

//指定查询字段
async function exec20() {
    let query = new AV.Query('Students');
    //query.select(['name','sex']);
    const res = await query.find();
    res.forEach(ele => {
        console.log(ele.toJSON());
    });
}

//非空值查询
async function exec21() {
    let query = new AV.Query('Todo');
    //query.exists('title');
    const res = await query.find() as AV.Object[];
    res.forEach(ele => {
        console.log(ele.toJSON());
    });
}

//总数
async function exec22() {
    let query = new AV.Query('Todo');
    query.equalTo('status',1);
    const res = await query.count();
    console.log(res);
}

//poiter
async function exec23() {
    let query = new AV.Query('Students');
    var toSchool = AV.Object.createWithoutData('Students','5b96394f1b69e6005b6d082d');

}

//relation 添加 移除
async function exec24() {
    let school = await new AV.Query('School').get('5b964b75570c350063fcd9b9') as AV.Object;
    //let student = await new AV.Query('Students').get('5b97841417d0090034ef588c') as AV.Object;
    //console.log(student.toJSON());
    let student =  AV.Object.createWithoutData('Students', '5b97841417d0090034ef588c');
    school.relation('students').add(student);
    //school.relation('students').remove(student);
    await school.save()
    //console.log(res);
    let relation = school.relation('students');
    let data = await relation.query().find() as AV.Object[];
    data.forEach(ele => {
        console.log(ele.get('name'))
    })
}

//pointer 一对一
async function exec25() {
    let student = await new AV.Query('Students').get('5b978fe9fb4ffe005c888b03') as AV.Object;
    let targetSchool = AV.Object.createWithoutData('School', '5b964b75570c350063fcd9b9');
    student.set('targetSchool', targetSchool);
    try {
       await student.save()
       let query = new AV.Query('Students');
       //关键代码
       query.include('targetSchool');
       const obj = await query.get('5b978fe9fb4ffe005c888b03') as AV.Object;
       console.log(obj.get('targetSchool').get('name'))
    } catch (err) {
        console.log(err)
    }
}

//对象中有某个字段为数据类型的查询
async function exec26() {
    let query = new AV.Query('Students')
    let booksFilter = ['math']
    query.containsAll('books', booksFilter)
    const obj = await query.find() as AV.Object[]
    obj.forEach( ele => {
        console.log( ele.toJSON() )
    })
}

exec26()
