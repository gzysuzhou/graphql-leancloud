
var router = require('express').Router()
import * as AV  from 'leancloud-storage'
import { Request, Response } from "express";

//查询 Todo 列表

router.get('/', async function(request: Request, response:Response, next: ()=>void){
    var query = new AV.Query('Todo')
    query.descending('createdAt')
    let results = await query.find()
    response.render('todos',{
        'title':'TODO列表',
        'todos':results
    })
    next()

})

router.post('/', async function(request: Request, response:Response, next: ()=>void){
    let content = await request.body.content
    let Todo = AV.Object.extend('Todo')
    let todo = new Todo()
    todo.set('content', content)
    await todo.save()
    response.redirect('/todos')
})

module.exports = router;