import * as AV from 'leancloud-storage'
import { BaseController } from "./BaseController";
import { GQRequest } from "../Application/GQRequest";
import { TodoConnection, UserConnection } from "../Entities/Connections";
import { TodoEntity } from "../Entities/TodoEntity";
import * as redis from 'redis'
import { UserEntity } from '../Entities/UserEntity';
const cacheClient = redis.createClient()
const find = AV.Query.prototype.find
AV.Query.prototype.find = function() {
    
    if (!this.useCache) {
        //return find.apply(this, arguments)
    }
    //console.log(this)
    
    return find.apply(this, arguments)
}
const first = AV.Query.prototype.first
AV.Query.prototype.first = function() {
    console.log("Query first")
    if (!this.useCache) {
        return first.apply(this, arguments)
    }
    return first.apply(this, arguments)
}

AV.Query.prototype.cache = function () {
    this.useCache = true
    return this
}
const save = AV.Object.prototype.save
AV.Object.prototype.save = function() {
    const className = this.className
    cacheClient.keys(className + "*", function(err, keys){
        if(!err){
            keys.map(async it => del(it))
        }
    })
    return save.apply(this, arguments)
}

const set = async function (cacheKey: string, cacheValue: any): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.set(cacheKey, cacheValue, function (err) {
            if (err) {
                rejector(err)
            }
            else {
                resolver()
            }
        })
    })
}

const get = async function (cacheKey: string): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.get(cacheKey, function (err, value) {
            if (err) {
                rejector(err)
            }
            else {
                resolver(value)
            }
        })
    })
}

const del = async function (cacheKey: string): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.del(cacheKey)
        resolver()
    })
}

const flush = async function (): Promise<any> {
    return new Promise((resolver) => {
        cacheClient.flushdb()
        resolver()
    })
}

export class TodoController extends BaseController {

    exportedMethods = [
        "hello",
        "users",
        "addTodo",
        "modifyTodo",
        "todo",
        "todos",
        "addRelateStudent",
        "modifyUser"

    ]

    async hello() {
        return "hello world"
    }

    async todo(args:{id:string}):Promise<TodoEntity> {
        const query = new AV.Query("Todo")
        query.equalTo("objectId", args.id)
        const obj = await query.find() as AV.Object[]
        return new TodoEntity(obj[0])
    }



    async todos(args: {
        input?:{
            id?: number,
            name?: string,
            type?: number
        }, skip?:number, limit?:number}): Promise<TodoConnection>{
        let query = TodoConnection.buildQuery()
        if (args.input) {
            if (args.input.id) {
                query.equalTo("objectId", args.input.id)
            }
            if (args.input.name) {
                query.contains("name", args.input.name)
            }
            if (args.input.type) {
                query.equalTo("type", args.input.type)
            }
        }
        query.cache()
        return new TodoConnection().exec(query, args.skip, args.limit)
    }

    async modifyUser(args: {id: string, input: {
        username: string
    }}): Promise<UserEntity> {
        const obj =  await AV.Object.createWithoutData("_User", args.id).fetch() as AV.User
        if(args.input.username) {
            obj.set("username", args.input.username)
            await obj.save()
        }
        return new UserEntity(obj)
    }

    async users(args:{id: string}): Promise<UserConnection>{
        const query = UserConnection.buildQuery()
        if( args.id) {
            query.equalTo("objectId", args.id)
        }
        return new UserConnection().exec(query, 0, 100)
    }

    //添加todo
    async addTodo(args: {name: string, type: number}, request: GQRequest): Promise<string> {
        // 1.先添加到Todo list
        let Todo = AV.Object.extend('Todo')
        let todo = new Todo()
        todo.set('name', args.name)
        todo.set('type', args.type)
        const res = await todo.save()
        return args.name
    }

    async addRelateStudent(args:{todoID:string,studentID:string}): Promise<boolean>{
        const obj = AV.Object.createWithoutData("Todo", args.todoID)
        obj.relation("student").add(AV.Object.createWithoutData("_User", args.studentID))
        await obj.save()
        return true
    }
      
    //修改todo
    async modifyTodo(args:{id: string, input?: {
        name?: string,
        type?: number
    }}): Promise<TodoEntity> {
        let obj = await AV.Object.createWithoutData("Todo", args.id).fetch()
        if(args.input) {
            if(args.input.name) {
                obj.set("name", args.input.name)
            }
            if(args.input.type) {
                obj.set("type", args.input.type)
            }
        }
        await obj.save()
        return new TodoEntity(obj)
    }

    
    
    //用户删除指定todo
    async deleteUserTodo(args: {userId: string, todoId: string}) {
        let query = new AV.Query('Demo_User')
        const obj = await query.equalTo('objectId', args.userId).first() as AV.Object
        if (obj) {
            let relation = obj.relation('todoList')
            const todo = await relation.query().get(args.todoId) as AV.Object
            if (todo) {
                await relation.remove(todo)
                await todo.destroy()
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }

}