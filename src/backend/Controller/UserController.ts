import * as AV from 'leancloud-storage'
import { BaseController } from "./BaseController";
import { GQRequest } from "../Application/GQRequest";

export class UserController extends BaseController {

    exportedMethods = [
        "hello",
        "login",
        "register",
        "uploadImage",
    ]

    async hello() {
        return "hello world"
    }

    async login(args: {mobile: number, password: string}) {
        let query = new AV.Query('Demo_User')
        const obj =  await query.equalTo('moblie', args.mobile).equalTo('password', args.password).first()
        if (obj) {
            return true
        } else {
            return false
        }
    }

    async register(args: {mobile: number, password: string}) {
        let query = new AV.Query('Demo_User')
        const obj =  await query.equalTo('moblie', args.mobile).first()
        if (!(obj instanceof AV.Object)) {
            let User =  AV.Object.extend('Demo_User')
            let user = new User()
            user.set('moblie', args.mobile)
            user.set('password', args.password)
            let res = await user.save()
            return true
        } else {
            return false
        }
    }

    async uploadImage(args: {userId: string, imageUrl: string,imageNameWithExtend: string}):Promise<boolean> {
        let file = AV.File.withURL(args.imageNameWithExtend, args.imageUrl)
        let fileObj = await file.save()
        if (typeof fileObj.url() !=='undefined') {
            let obj = AV.Object.createWithoutData('Demo_User', args.userId) as AV.Object
            obj.set('avatar', args.imageUrl)
            await obj.save()
            return true
        } else {
            return false
        }
    }
}