import { GQRequest } from "../Application/GQRequest";

export class BaseController {

    exportedMethods: string[] = []
    hostApplication: any

    attach(toApplication: any) {
        this.hostApplication = toApplication
        this.exportedMethods.forEach(it => {
            toApplication[it] = (this as any)[it].bind(this)
        })
    }

    protected checkAdmin(request: GQRequest) {
        if (!request.user) { throw Error("Token 无效") }
        if (request.user.get('isAdmin') !== true) { throw Error("你不是管理员") }
    }

}