import * as AV from 'leancloud-storage'
import { Request } from "express";

export interface GQRequest extends Request {
    user?: AV.User
}