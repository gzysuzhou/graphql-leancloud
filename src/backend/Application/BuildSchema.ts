var { buildSchema } = require('graphql');

// 使用 GraphQL Schema Language 创建一个 schema
export  var schema = buildSchema(`

  type Todo {
    id: String
    name: String
    type: Int
    student: UserConnection
  }

  type UserConnection {
    nodes: [User] 
    pageInfo: PageInfo! #页码信息
    totalCount: Int #结果总数
  }

  type User {
    id: String
    username: String
  }

  type PageInfo {
    hasNextPage: Boolean! #是否存在下一页
    nextCursor: Int #下一页的游标
  }

  type TodoConnection {
    nodes: [Todo] #Todo列表
    pageInfo: PageInfo! #页码信息
    totalCount: Int #结果总数
  }

  input TodoListSearch {
    id: String
    name: String
    type: Int
  }

  type Query {
    hello(id:Int): String
    users(id: String): UserConnection
    login(mobile: Int!, password: String!):Boolean
    todo(id: String): Todo
    todos(input: TodoListSearch, skip: Int, limit: Int): TodoConnection
  }

  input TodoModify {
    name: String
    type: Int
  }

  input UserModify {
    username: String
  }

  type Mutation {
    register(mobile: Int!, password: String!): Boolean
    addTodo(name: String! type: Int!): String
    modifyTodo(id: String!, input: TodoModify): Todo
    deleteUserTodo(userId: String!, todoId: String!): Boolean
    uploadImage(userId: String!, imageUrl: String!, imageNameWithExtend: String!): Boolean
    addRelateStudent(todoID: String!, studentID: String!): Boolean
    modifyUser(id: String! input: UserModify): User
  }
`);
