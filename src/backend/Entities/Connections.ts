import { PageInfo } from "./common"
import { Query } from "leancloud-storage"
import { UserEntity } from "./UserEntity"
import { TodoEntity } from "./TodoEntity"
import { doTask } from "../Utils/TaskQueue";
import { CacheCore } from "../Utils/Cache"

import * as redis from 'redis'
const cacheClient = redis.createClient()

cacheClient.on('error', (error) => { console.error(error) })

const set = async function (cacheKey: string, cacheValue: any): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.set(cacheKey, cacheValue, function (err) {
            if (err) {
                rejector(err)
            }
            else {
                resolver()
            }
        })
    })
}

const get = async function (cacheKey: string): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.get(cacheKey, function (err, value) {
            if (err) {
                rejector(err)
            }
            else {
                resolver(value)
            }
        })
    })
}

const del = async function (cacheKey: string): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.del(cacheKey)
        resolver()
    })
}

const flush = async function (): Promise<any> {
    return new Promise((resolver) => {
        cacheClient.flushdb()
        resolver()
    })
}

export class Connection<T> {
    nodes: T[]
    pageInfo: PageInfo
    totalCount: number

    protected clazz: any

    async exec(query: Query<any>, skip: number = 0, limit: number = 30): Promise<this> {
        
        this.totalCount = await query.count() as number
        query.skip(skip)
        query.limit(limit)
        //console.log(query.className)
        //console.log(cacheKey)
        //flush()
        //console.log(query)
        const cacheKey =  Connection.cacheKey(query)
        //await del('Todo{"objectId":"5baef065fb4ffe006952d367"}_0_30')
        //await del("_User_Pointer_to_Todo_id_5baef065fb4ffe006952d367_0_10")
        //console.log(cacheKey)
        if (query.useCache) {
            //await del("_User_Pointer_to_Todo_id_5baef065fb4ffe006952d367_0_100")
            //await del("Todo_0_1")
            const cacheData = await get(cacheKey)
            console.log(cacheKey)
            //console.log(cacheData)
            if (cacheData) {
                const cacheObjects = JSON.parse(cacheData)
                console.log("use cache")
                const obj = CacheCore.createObjectFromJSON(JSON.stringify(cacheObjects[0]))
                //console.log(cacheObjects)
                let Entity = obj.className + "Entity"
                //console.log(Entity)
                if (Entity == 'TodoEntity') {
                    this.nodes = cacheObjects.map(it => new TodoEntity(CacheCore.createObjectFromJSON(JSON.stringify(it))))
                } 
                else if (Entity == '_UserEntity') {
                    this.nodes = cacheObjects.map(it => new UserEntity(CacheCore.createObjectFromJSON(JSON.stringify(it)) as AV.User))
                }
            } else {
                
                const objects = (await doTask<AV.Object[]>(() => {
                    const results = query.find()
                    return results
                }))
                this.nodes = objects.map(it => new (this.clazz)(it)).filter(it => {
                    if (it.verify instanceof Function) {
                        return it.verify()
                    }
                    return true
                })
                //flush()
                console.log("useQuery1")
                if (query.useCache) {
                    //console.log(objects)
                    //console.log(query.className)
                    set(cacheKey, JSON.stringify(objects.map(it => it.toFullJSON())))
                }
            }
        } else {
            const objects = (await doTask<AV.Object[]>(() => {
                const results = query.find()
                return results
            }))
            this.nodes = objects.map(it => new (this.clazz)(it)).filter(it => {
                if (it.verify instanceof Function) {
                    return it.verify()
                }
                return true
            })
            //flush()
            console.log("useQuery2")
        }
        //console.log(this)
        this.pageInfo = new PageInfo(this.nodes.length >= limit, skip + limit)
        return this
    }

    static cacheKey(query: Query<any>){
        let cacheKey = query.className
        if (JSON.stringify(query._select) !=="[]") {
            cacheKey += "_" + query._select.join("_")
        }
        
        if (JSON.stringify(query._include) !=="[]") {
            cacheKey += "_" + query._include.join("_")
        }
        
        if (JSON.stringify(query._where) !=="{}") {
            if (query._where.$relatedTo && query._where.$relatedTo.object) {
                 //console.log(query._where.$relatedTo.object)
                // console.log(query._where.$relatedTo.object.__type)
                // console.log(query._where.$relatedTo.object.className)
                // console.log(query._where.$relatedTo.object.objectId)
                if (query._where.$relatedTo.object.__type === "Pointer"){
                    cacheKey += "_" + "Pointer_to_"
                                + query._where.$relatedTo.object.className + "_"
                                + "id_" + query._where.$relatedTo.object.objectId
                }
            } else {
                
                cacheKey += JSON.stringify(query._where)
            }
        }
        
        cacheKey += "_" + query._skip + "_" + query._limit
        //console.log(query)
       return cacheKey
    }
}

export class TodoConnection extends Connection<TodoEntity> {
    static buildQuery(q: Query<any> | undefined = undefined): Query<any> {
        const query = q || new Query("Todo")
        return query
    }

    clazz = TodoEntity
}

export class UserConnection extends Connection<UserEntity> {
    static buildQuery(q: Query<any> | undefined = undefined): Query<any> {
        const query = q || new Query("_User")
        return query
    }

    clazz = UserEntity
}

