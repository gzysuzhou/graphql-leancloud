import * as AV from 'leancloud-storage'
import { PageInfo } from './Common';
import { GQRequest } from '../Application/GQRequest';
import { UserEntity } from './UserEntity';
import { TodoConnection, UserConnection } from './Connections';
import { SSL_OP_TLS_BLOCK_PADDING_BUG } from 'constants';

export class TodoEntity {

    public get id(): string {
        return this.obj.id
    }

    public get name(): string {
        return this.obj.get("name")
    }

    public get type(): number {
        return this.obj.get("type")
    }

    public get student(): Promise<UserConnection | undefined> {
        //const query = UserConnection.buildQuery(this.obj.relation("student").query())
        const query = UserConnection.buildQuery(this.obj.relation("student").query())
        query.cache()
        //console.log(this.obj.relation("student").query().find())
        //console.log(query)
        //console.log(this.obj.relation("student").query())
        //console.log(query)
        return new UserConnection().exec(query, 0, 10)
    }

    constructor(readonly obj: AV.Object) {
        //console.log(obj)
    }
}
