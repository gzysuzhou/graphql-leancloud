export class PageInfo {

    constructor(readonly hasNextPage: boolean, readonly nextCursor?: number) {
        if (!hasNextPage) {
            this.nextCursor = undefined
        }
    }

}