import * as AV from 'leancloud-storage'
import { PageInfo } from './Common';
import { GQRequest } from '../Application/GQRequest';

export class UserEntity {
    public get id(): string {
        return this.obj.id
    }

    public get mobilePhone(): number {
        return this.obj.get("moblePhone")
    }

    public get username(): string {
        return this.obj.get("username")
    }

    constructor(readonly obj: AV.User) { }
}