import { Request, Response } from "express";
import * as graphqlHTTP from "express-graphql";
import * as AV from 'leancloud-storage'
import { schema } from "./Application/BuildSchema";
import { GQRequest } from "./Application/GQRequest";
import { UserEntity } from "./Entities/UserEntity";
import { TodoEntity } from "./Entities/TodoEntity";
import { UserController } from "./Controller/UserController";
import { TodoConnection } from "./Entities/Connections";
import { TodoController } from "./Controller/TodoController";

export class BackendApplication {

    static register(app: any) {
        app.use('/backend', async function (request: GQRequest, response: Response, next: () => void) {
            const token = request.headers['token']
            if (typeof token === "string") {
                try {
                    let user = await AV.User.become(token)
                    if (user.get('isAdmin') === true) {
                        request.user = user
                    }
                } catch (error) { }
            }
            next()
        })
        
        app.use('/backend', graphqlHTTP({
            schema,
            rootValue: new BackendApplication,
            graphiql: true,
        }));
    }

    userController = new UserController
    todoController = new TodoController
    constructor() {
        this.userController.attach(this)
        this.todoController.attach(this)
    }


}