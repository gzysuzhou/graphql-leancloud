import AV = require("leancloud-storage");
import * as redis from 'redis'
import { GQRequest } from '../Application/GQRequest';
import { TaskQueue, doTask } from "./TaskQueue";

const cacheClient = redis.createClient()

cacheClient.on('error', (error) => { console.error(error) })

const set = async function (cacheKey: string, cacheValue: any): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.set(cacheKey, cacheValue, function (err) {
            if (err) {
                rejector(err)
            }
            else {
                resolver()
            }
        })
    })
}

const get = async function (cacheKey: string): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.get(cacheKey, function (err, value) {
            if (err) {
                rejector(err)
            }
            else {
                resolver(value)
            }
        })
    })
}

const del = async function (cacheKey: string): Promise<any> {
    return new Promise((resolver, rejector) => {
        cacheClient.del(cacheKey)
        resolver()
    })
}

const flush = async function (): Promise<any> {
    return new Promise((resolver) => {
        cacheClient.flushdb()
        resolver()
    })
}

export class CacheCore {

    static flush() {
        flush()
    }

    static createObjectFromJSON(value: string): AV.Object {
        const json = JSON.parse(value)
        const obj = AV.Object.createWithoutData(json.className, json.objectId)
        for (const key in json) {
            if (key === "objectId") {
                continue
            }
            if (key === "createdAt") {
                continue
            }
            else if (key === "updatedAt") {
                continue
            }
            else if (typeof json[key] === "string") {
                obj.set(key, json[key])
            }
            else if (typeof json[key] === "number") {
                obj.set(key, json[key])
            }
            else if (typeof json[key] === "boolean") {
                obj.set(key, json[key])
            }
            else if (json[key] instanceof Array) {
                obj.set(key, json[key])
            }
            else if (json[key] instanceof Object && json[key] != null && json[key]['__type'] === 'Pointer') {
                obj.set(key, AV.Object.createWithoutData(json[key]['className'], json[key]['objectId']))
            }
            else if (json[key] instanceof Object && json[key] != null && json[key]['__type'] === 'File') {
                const file = AV.File.withURL(json[key]['name'], json[key]['url'])
                obj.set(key, file)
            }
            else if (!isNaN(new Date(json[key]).getTime())) {
                obj.set(key, new Date(json[key]))
            }
        }
        return obj
    }

    static clear_character_viewerIsJiBanning(obj: AV.Object, user: AV.User) {
        const cacheKey = "character_viewerIsJiBanning:" + user.id + ":" + obj.id
        del(cacheKey)
    }

    static async post_viewerHasStarred(obj: AV.Object, request: GQRequest): Promise<boolean> {
        if (request.user === undefined) { return false }
        const cacheKey = "post_viewerHasStarred:" + request.user.id + ":" + obj.id
        const result = await get(cacheKey)
        if (result !== null) {
            return result === "true"
        }
        else {
            const result = await doTask(() => {
                const query = obj.relation('starUser').query()
                query.equalTo('objectId', request.user.id)
                return query.first()
            }) != undefined
            await set(cacheKey, result)
            return result
        }
    }

    static clear_post_viewerHasStarred(obj: AV.Object, request: GQRequest) {
        if (request.user === undefined) { return }
        const cacheKey = "post_viewerHasStarred:" + request.user.id + ":" + obj.id
        del(cacheKey)
    }

    static async clubpost_starCount(obj: AV.Object): Promise<number> {
        const cacheKey = "clubpost_starCount:" + obj.id
        const result = await get(cacheKey)
        if (result !== null) {
            return parseInt(result)
        }
        else {
            const result = await doTask<number>(() => {
                const query = obj.relation('starUser').query()
                return query.count()
            })
            await set(cacheKey, result)
            return result
        }
    }

    static async update_clubpost_starCount(obj: AV.Object): Promise<any> {
        const cacheKey = "clubpost_starCount:" + obj.id
        const result = await doTask<number>(() => {
            const query = obj.relation('starUser').query()
            return query.count()
        })
        await set(cacheKey, result)
    }

    static async clubpost_commentCount(obj: AV.Object): Promise<number> {
        const cacheKey = "clubpost_commentCount:" + obj.id
        const result = await get(cacheKey)
        if (result !== null) {
            return parseInt(result)
        }
        else {
            const result = await doTask<number>(() => {
                const query = obj.relation('subPosts').query()
                return query.count()
            })
            await set(cacheKey, result)
            return result
        }
    }

    static async update_clubpost_commentCount(obj: AV.Object): Promise<any> {
        const cacheKey = "clubpost_commentCount:" + obj.id
        const result = await doTask<number>(() => {
            const query = obj.relation('subPosts').query()
            return query.count()
        })
        await set(cacheKey, result)
    }

    static async clubpost_viewerHasStarred(obj: AV.Object, request: GQRequest): Promise<boolean> {
        if (request.user === undefined) { return false }
        const cacheKey = "clubpost_viewerHasStarred:" + request.user.id + ":" + obj.id
        const result = await get(cacheKey)
        if (result !== null) {
            return result === "true"
        }
        else {
            const result = await doTask(() => {
                const query = obj.relation('starUser').query()
                query.equalTo('objectId', request.user.id)
                return query.first()
            }) != undefined
            await set(cacheKey, result)
            return result
        }
    }

    static clear_clubpost_viewerHasStarred(obj: AV.Object, request: GQRequest) {
        if (request.user === undefined) { return }
        const cacheKey = "clubpost_viewerHasStarred:" + request.user.id + ":" + obj.id
        del(cacheKey)
    }

    static async user_viewerIsFollowing(obj: AV.User, request: GQRequest): Promise<boolean> {
        if (request.user === undefined) { return false }
        const cacheKey = "user_viewerIsFollowing:" + request.user.id + ":" + obj.id
        const result = await get(cacheKey)
        if (result !== null) {
            return result === "true"
        }
        else {
            const result = await doTask(() => {
                const query = obj.followerQuery()
                query.equalTo("follower", request.user)
                return query.count()
            }) > 0
            await set(cacheKey, result)
            return result
        }
    }

    static clear_user_viewerIsFollowing(obj: AV.User, request: GQRequest) {
        if (request.user === undefined) { return }
        const cacheKey = "user_viewerIsFollowing:" + request.user.id + ":" + obj.id
        del(cacheKey)
    }

    static pubsub_addListenner(event: string, deviceToken: string) {
        const cacheKey = "pubsub_" + event
        cacheClient.SADD(cacheKey, deviceToken)
    }

    static pubsub_removeListenner(event: string, deviceToken: string) {
        const cacheKey = "pubsub_" + event
        cacheClient.SREM(cacheKey, deviceToken)
    }

    static async pubsub_listenningTokens(event: string): Promise<string[]> {
        const cacheKey = "pubsub_" + event
        return new Promise<string[]>((resolver) => {
            cacheClient.SMEMBERS(cacheKey, (err, result) => {
                if (err) {
                    resolver([])
                }
                else {
                    resolver(result)
                }
            })
        })
    }

    static pubsub_clear() {
        cacheClient.keys("pubsub_*", (err, keys) => {
            cacheClient.del.apply(cacheClient, keys)
        })
    }

}

export class MemoryCache {

    static shared = new MemoryCache

    private valueMapping: { [key: string]: any } = {}

    private valueOutDate: { [key: string]: number } = {}

    constructor() {
        setInterval(() => {
            Object.keys(this.valueOutDate).forEach((objKey) => {
                if (this.valueOutDate[objKey] < Date.now()) {
                    delete this.valueMapping[objKey]
                    delete this.valueOutDate[objKey]
                }
            })
        }, 60000)
    }

    get(key: string): any | undefined {
        if (this.valueMapping[key] && this.valueOutDate[key] > Date.now()) {
            return this.valueMapping[key]
        }
        else {
            return undefined
        }
    }

    set(key: string, value: any, expires: number) {
        this.valueMapping[key] = value
        this.valueOutDate[key] = Date.now() + expires * 1000
    }

}