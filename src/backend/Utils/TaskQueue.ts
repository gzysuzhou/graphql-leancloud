const PQueue = require('p-queue');
const queue = new PQueue({ concurrency: parseInt(process.env.maxConcurrent) || 2 });

export class TaskQueue {

    static shared = new TaskQueue

    do<T>(task: any): Promise<T> {
        return queue.add(task)
    }

}

export const doTask = TaskQueue.shared.do