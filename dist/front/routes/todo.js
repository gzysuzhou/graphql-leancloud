"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var router = require('express').Router();
const AV = require("leancloud-storage");
//查询 Todo 列表
router.get('/', function (request, response, next) {
    return __awaiter(this, void 0, void 0, function* () {
        var query = new AV.Query('Todo');
        query.descending('createdAt');
        let results = yield query.find();
        response.render('todos', {
            'title': 'TODO列表',
            'todos': results
        });
        next();
    });
});
router.post('/', function (request, response, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let content = yield request.body.content;
        let Todo = AV.Object.extend('Todo');
        let todo = new Todo();
        todo.set('content', content);
        yield todo.save();
        response.redirect('/todos');
    });
});
module.exports = router;
