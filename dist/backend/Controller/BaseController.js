"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseController {
    constructor() {
        this.exportedMethods = [];
    }
    attach(toApplication) {
        this.hostApplication = toApplication;
        this.exportedMethods.forEach(it => {
            toApplication[it] = this[it].bind(this);
        });
    }
    checkAdmin(request) {
        if (!request.user) {
            throw Error("Token 无效");
        }
        if (request.user.get('isAdmin') !== true) {
            throw Error("你不是管理员");
        }
    }
}
exports.BaseController = BaseController;
