"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AV = require("leancloud-storage");
const BaseController_1 = require("./BaseController");
class UserController extends BaseController_1.BaseController {
    constructor() {
        super(...arguments);
        this.exportedMethods = [
            "hello",
            "login",
            "register",
            "uploadImage",
        ];
    }
    hello() {
        return __awaiter(this, void 0, void 0, function* () {
            return "hello world";
        });
    }
    login(args) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = new AV.Query('Demo_User');
            const obj = yield query.equalTo('moblie', args.mobile).equalTo('password', args.password).first();
            if (obj) {
                return true;
            }
            else {
                return false;
            }
        });
    }
    register(args) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = new AV.Query('Demo_User');
            const obj = yield query.equalTo('moblie', args.mobile).first();
            if (!(obj instanceof AV.Object)) {
                let User = AV.Object.extend('Demo_User');
                let user = new User();
                user.set('moblie', args.mobile);
                user.set('password', args.password);
                let res = yield user.save();
                return true;
            }
            else {
                return false;
            }
        });
    }
    uploadImage(args) {
        return __awaiter(this, void 0, void 0, function* () {
            let file = AV.File.withURL(args.imageNameWithExtend, args.imageUrl);
            let fileObj = yield file.save();
            if (typeof fileObj.url() !== 'undefined') {
                let obj = AV.Object.createWithoutData('Demo_User', args.userId);
                obj.set('avatar', args.imageUrl);
                yield obj.save();
                return true;
            }
            else {
                return false;
            }
        });
    }
}
exports.UserController = UserController;
