"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AV = require("leancloud-storage");
const BaseController_1 = require("./BaseController");
const Connections_1 = require("../Entities/Connections");
const TodoEntity_1 = require("../Entities/TodoEntity");
const redis = require("redis");
const UserEntity_1 = require("../Entities/UserEntity");
const cacheClient = redis.createClient();
const find = AV.Query.prototype.find;
AV.Query.prototype.find = function () {
    if (!this.useCache) {
        //return find.apply(this, arguments)
    }
    //console.log(this)
    return find.apply(this, arguments);
};
const first = AV.Query.prototype.first;
AV.Query.prototype.first = function () {
    console.log("Query first");
    if (!this.useCache) {
        return first.apply(this, arguments);
    }
    return first.apply(this, arguments);
};
AV.Query.prototype.cache = function () {
    this.useCache = true;
    return this;
};
const save = AV.Object.prototype.save;
AV.Object.prototype.save = function () {
    const className = this.className;
    cacheClient.keys(className + "*", function (err, keys) {
        if (!err) {
            keys.map((it) => __awaiter(this, void 0, void 0, function* () { return del(it); }));
        }
    });
    return save.apply(this, arguments);
};
const set = function (cacheKey, cacheValue) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.set(cacheKey, cacheValue, function (err) {
                if (err) {
                    rejector(err);
                }
                else {
                    resolver();
                }
            });
        });
    });
};
const get = function (cacheKey) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.get(cacheKey, function (err, value) {
                if (err) {
                    rejector(err);
                }
                else {
                    resolver(value);
                }
            });
        });
    });
};
const del = function (cacheKey) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.del(cacheKey);
            resolver();
        });
    });
};
const flush = function () {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver) => {
            cacheClient.flushdb();
            resolver();
        });
    });
};
class TodoController extends BaseController_1.BaseController {
    constructor() {
        super(...arguments);
        this.exportedMethods = [
            "hello",
            "users",
            "addTodo",
            "modifyTodo",
            "todo",
            "todos",
            "addRelateStudent",
            "modifyUser"
        ];
    }
    hello() {
        return __awaiter(this, void 0, void 0, function* () {
            return "hello world";
        });
    }
    todo(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = new AV.Query("Todo");
            query.equalTo("objectId", args.id);
            const obj = yield query.find();
            return new TodoEntity_1.TodoEntity(obj[0]);
        });
    }
    todos(args) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = Connections_1.TodoConnection.buildQuery();
            if (args.input) {
                if (args.input.id) {
                    query.equalTo("objectId", args.input.id);
                }
                if (args.input.name) {
                    query.contains("name", args.input.name);
                }
                if (args.input.type) {
                    query.equalTo("type", args.input.type);
                }
            }
            query.cache();
            return new Connections_1.TodoConnection().exec(query, args.skip, args.limit);
        });
    }
    modifyUser(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const obj = yield AV.Object.createWithoutData("_User", args.id).fetch();
            if (args.input.username) {
                obj.set("username", args.input.username);
                yield obj.save();
            }
            return new UserEntity_1.UserEntity(obj);
        });
    }
    users(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = Connections_1.UserConnection.buildQuery();
            if (args.id) {
                query.equalTo("objectId", args.id);
            }
            return new Connections_1.UserConnection().exec(query, 0, 100);
        });
    }
    //添加todo
    addTodo(args, request) {
        return __awaiter(this, void 0, void 0, function* () {
            // 1.先添加到Todo list
            let Todo = AV.Object.extend('Todo');
            let todo = new Todo();
            todo.set('name', args.name);
            todo.set('type', args.type);
            const res = yield todo.save();
            return args.name;
        });
    }
    addRelateStudent(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const obj = AV.Object.createWithoutData("Todo", args.todoID);
            obj.relation("student").add(AV.Object.createWithoutData("_User", args.studentID));
            yield obj.save();
            return true;
        });
    }
    //修改todo
    modifyTodo(args) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = yield AV.Object.createWithoutData("Todo", args.id).fetch();
            if (args.input) {
                if (args.input.name) {
                    obj.set("name", args.input.name);
                }
                if (args.input.type) {
                    obj.set("type", args.input.type);
                }
            }
            yield obj.save();
            return new TodoEntity_1.TodoEntity(obj);
        });
    }
    //用户删除指定todo
    deleteUserTodo(args) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = new AV.Query('Demo_User');
            const obj = yield query.equalTo('objectId', args.userId).first();
            if (obj) {
                let relation = obj.relation('todoList');
                const todo = yield relation.query().get(args.todoId);
                if (todo) {
                    yield relation.remove(todo);
                    yield todo.destroy();
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        });
    }
}
exports.TodoController = TodoController;
