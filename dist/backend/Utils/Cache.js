"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AV = require("leancloud-storage");
const redis = require("redis");
const TaskQueue_1 = require("./TaskQueue");
const cacheClient = redis.createClient();
cacheClient.on('error', (error) => { console.error(error); });
const set = function (cacheKey, cacheValue) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.set(cacheKey, cacheValue, function (err) {
                if (err) {
                    rejector(err);
                }
                else {
                    resolver();
                }
            });
        });
    });
};
const get = function (cacheKey) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.get(cacheKey, function (err, value) {
                if (err) {
                    rejector(err);
                }
                else {
                    resolver(value);
                }
            });
        });
    });
};
const del = function (cacheKey) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.del(cacheKey);
            resolver();
        });
    });
};
const flush = function () {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver) => {
            cacheClient.flushdb();
            resolver();
        });
    });
};
class CacheCore {
    static flush() {
        flush();
    }
    static createObjectFromJSON(value) {
        const json = JSON.parse(value);
        const obj = AV.Object.createWithoutData(json.className, json.objectId);
        for (const key in json) {
            if (key === "objectId") {
                continue;
            }
            if (key === "createdAt") {
                continue;
            }
            else if (key === "updatedAt") {
                continue;
            }
            else if (typeof json[key] === "string") {
                obj.set(key, json[key]);
            }
            else if (typeof json[key] === "number") {
                obj.set(key, json[key]);
            }
            else if (typeof json[key] === "boolean") {
                obj.set(key, json[key]);
            }
            else if (json[key] instanceof Array) {
                obj.set(key, json[key]);
            }
            else if (json[key] instanceof Object && json[key] != null && json[key]['__type'] === 'Pointer') {
                obj.set(key, AV.Object.createWithoutData(json[key]['className'], json[key]['objectId']));
            }
            else if (json[key] instanceof Object && json[key] != null && json[key]['__type'] === 'File') {
                const file = AV.File.withURL(json[key]['name'], json[key]['url']);
                obj.set(key, file);
            }
            else if (!isNaN(new Date(json[key]).getTime())) {
                obj.set(key, new Date(json[key]));
            }
        }
        return obj;
    }
    static clear_character_viewerIsJiBanning(obj, user) {
        const cacheKey = "character_viewerIsJiBanning:" + user.id + ":" + obj.id;
        del(cacheKey);
    }
    static post_viewerHasStarred(obj, request) {
        return __awaiter(this, void 0, void 0, function* () {
            if (request.user === undefined) {
                return false;
            }
            const cacheKey = "post_viewerHasStarred:" + request.user.id + ":" + obj.id;
            const result = yield get(cacheKey);
            if (result !== null) {
                return result === "true";
            }
            else {
                const result = (yield TaskQueue_1.doTask(() => {
                    const query = obj.relation('starUser').query();
                    query.equalTo('objectId', request.user.id);
                    return query.first();
                })) != undefined;
                yield set(cacheKey, result);
                return result;
            }
        });
    }
    static clear_post_viewerHasStarred(obj, request) {
        if (request.user === undefined) {
            return;
        }
        const cacheKey = "post_viewerHasStarred:" + request.user.id + ":" + obj.id;
        del(cacheKey);
    }
    static clubpost_starCount(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const cacheKey = "clubpost_starCount:" + obj.id;
            const result = yield get(cacheKey);
            if (result !== null) {
                return parseInt(result);
            }
            else {
                const result = yield TaskQueue_1.doTask(() => {
                    const query = obj.relation('starUser').query();
                    return query.count();
                });
                yield set(cacheKey, result);
                return result;
            }
        });
    }
    static update_clubpost_starCount(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const cacheKey = "clubpost_starCount:" + obj.id;
            const result = yield TaskQueue_1.doTask(() => {
                const query = obj.relation('starUser').query();
                return query.count();
            });
            yield set(cacheKey, result);
        });
    }
    static clubpost_commentCount(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const cacheKey = "clubpost_commentCount:" + obj.id;
            const result = yield get(cacheKey);
            if (result !== null) {
                return parseInt(result);
            }
            else {
                const result = yield TaskQueue_1.doTask(() => {
                    const query = obj.relation('subPosts').query();
                    return query.count();
                });
                yield set(cacheKey, result);
                return result;
            }
        });
    }
    static update_clubpost_commentCount(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const cacheKey = "clubpost_commentCount:" + obj.id;
            const result = yield TaskQueue_1.doTask(() => {
                const query = obj.relation('subPosts').query();
                return query.count();
            });
            yield set(cacheKey, result);
        });
    }
    static clubpost_viewerHasStarred(obj, request) {
        return __awaiter(this, void 0, void 0, function* () {
            if (request.user === undefined) {
                return false;
            }
            const cacheKey = "clubpost_viewerHasStarred:" + request.user.id + ":" + obj.id;
            const result = yield get(cacheKey);
            if (result !== null) {
                return result === "true";
            }
            else {
                const result = (yield TaskQueue_1.doTask(() => {
                    const query = obj.relation('starUser').query();
                    query.equalTo('objectId', request.user.id);
                    return query.first();
                })) != undefined;
                yield set(cacheKey, result);
                return result;
            }
        });
    }
    static clear_clubpost_viewerHasStarred(obj, request) {
        if (request.user === undefined) {
            return;
        }
        const cacheKey = "clubpost_viewerHasStarred:" + request.user.id + ":" + obj.id;
        del(cacheKey);
    }
    static user_viewerIsFollowing(obj, request) {
        return __awaiter(this, void 0, void 0, function* () {
            if (request.user === undefined) {
                return false;
            }
            const cacheKey = "user_viewerIsFollowing:" + request.user.id + ":" + obj.id;
            const result = yield get(cacheKey);
            if (result !== null) {
                return result === "true";
            }
            else {
                const result = (yield TaskQueue_1.doTask(() => {
                    const query = obj.followerQuery();
                    query.equalTo("follower", request.user);
                    return query.count();
                })) > 0;
                yield set(cacheKey, result);
                return result;
            }
        });
    }
    static clear_user_viewerIsFollowing(obj, request) {
        if (request.user === undefined) {
            return;
        }
        const cacheKey = "user_viewerIsFollowing:" + request.user.id + ":" + obj.id;
        del(cacheKey);
    }
    static pubsub_addListenner(event, deviceToken) {
        const cacheKey = "pubsub_" + event;
        cacheClient.SADD(cacheKey, deviceToken);
    }
    static pubsub_removeListenner(event, deviceToken) {
        const cacheKey = "pubsub_" + event;
        cacheClient.SREM(cacheKey, deviceToken);
    }
    static pubsub_listenningTokens(event) {
        return __awaiter(this, void 0, void 0, function* () {
            const cacheKey = "pubsub_" + event;
            return new Promise((resolver) => {
                cacheClient.SMEMBERS(cacheKey, (err, result) => {
                    if (err) {
                        resolver([]);
                    }
                    else {
                        resolver(result);
                    }
                });
            });
        });
    }
    static pubsub_clear() {
        cacheClient.keys("pubsub_*", (err, keys) => {
            cacheClient.del.apply(cacheClient, keys);
        });
    }
}
exports.CacheCore = CacheCore;
class MemoryCache {
    constructor() {
        this.valueMapping = {};
        this.valueOutDate = {};
        setInterval(() => {
            Object.keys(this.valueOutDate).forEach((objKey) => {
                if (this.valueOutDate[objKey] < Date.now()) {
                    delete this.valueMapping[objKey];
                    delete this.valueOutDate[objKey];
                }
            });
        }, 60000);
    }
    get(key) {
        if (this.valueMapping[key] && this.valueOutDate[key] > Date.now()) {
            return this.valueMapping[key];
        }
        else {
            return undefined;
        }
    }
    set(key, value, expires) {
        this.valueMapping[key] = value;
        this.valueOutDate[key] = Date.now() + expires * 1000;
    }
}
MemoryCache.shared = new MemoryCache;
exports.MemoryCache = MemoryCache;
