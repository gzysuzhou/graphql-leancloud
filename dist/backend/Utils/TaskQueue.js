"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PQueue = require('p-queue');
const queue = new PQueue({ concurrency: parseInt(process.env.maxConcurrent) || 2 });
class TaskQueue {
    do(task) {
        return queue.add(task);
    }
}
TaskQueue.shared = new TaskQueue;
exports.TaskQueue = TaskQueue;
exports.doTask = TaskQueue.shared.do;
