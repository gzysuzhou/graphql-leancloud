"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphqlHTTP = require("express-graphql");
const AV = require("leancloud-storage");
const BuildSchema_1 = require("./Application/BuildSchema");
const UserController_1 = require("./Controller/UserController");
const TodoController_1 = require("./Controller/TodoController");
class BackendApplication {
    constructor() {
        this.userController = new UserController_1.UserController;
        this.todoController = new TodoController_1.TodoController;
        this.userController.attach(this);
        this.todoController.attach(this);
    }
    static register(app) {
        app.use('/backend', function (request, response, next) {
            return __awaiter(this, void 0, void 0, function* () {
                const token = request.headers['token'];
                if (typeof token === "string") {
                    try {
                        let user = yield AV.User.become(token);
                        if (user.get('isAdmin') === true) {
                            request.user = user;
                        }
                    }
                    catch (error) { }
                }
                next();
            });
        });
        app.use('/backend', graphqlHTTP({
            schema: BuildSchema_1.schema,
            rootValue: new BackendApplication,
            graphiql: true,
        }));
    }
}
exports.BackendApplication = BackendApplication;
