"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("./common");
const leancloud_storage_1 = require("leancloud-storage");
const UserEntity_1 = require("./UserEntity");
const TodoEntity_1 = require("./TodoEntity");
const TaskQueue_1 = require("../Utils/TaskQueue");
const Cache_1 = require("../Utils/Cache");
const redis = require("redis");
const cacheClient = redis.createClient();
cacheClient.on('error', (error) => { console.error(error); });
const set = function (cacheKey, cacheValue) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.set(cacheKey, cacheValue, function (err) {
                if (err) {
                    rejector(err);
                }
                else {
                    resolver();
                }
            });
        });
    });
};
const get = function (cacheKey) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.get(cacheKey, function (err, value) {
                if (err) {
                    rejector(err);
                }
                else {
                    resolver(value);
                }
            });
        });
    });
};
const del = function (cacheKey) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver, rejector) => {
            cacheClient.del(cacheKey);
            resolver();
        });
    });
};
const flush = function () {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolver) => {
            cacheClient.flushdb();
            resolver();
        });
    });
};
class Connection {
    exec(query, skip = 0, limit = 30) {
        return __awaiter(this, void 0, void 0, function* () {
            this.totalCount = (yield query.count());
            query.skip(skip);
            query.limit(limit);
            //console.log(query.className)
            //console.log(cacheKey)
            //flush()
            //console.log(query)
            const cacheKey = Connection.cacheKey(query);
            //await del('Todo{"objectId":"5baef065fb4ffe006952d367"}_0_30')
            //await del("_User_Pointer_to_Todo_id_5baef065fb4ffe006952d367_0_10")
            //console.log(cacheKey)
            if (query.useCache) {
                //await del("_User_Pointer_to_Todo_id_5baef065fb4ffe006952d367_0_100")
                //await del("Todo_0_1")
                const cacheData = yield get(cacheKey);
                console.log(cacheKey);
                //console.log(cacheData)
                if (cacheData) {
                    const cacheObjects = JSON.parse(cacheData);
                    console.log("use cache");
                    const obj = Cache_1.CacheCore.createObjectFromJSON(JSON.stringify(cacheObjects[0]));
                    //console.log(cacheObjects)
                    let Entity = obj.className + "Entity";
                    //console.log(Entity)
                    if (Entity == 'TodoEntity') {
                        this.nodes = cacheObjects.map(it => new TodoEntity_1.TodoEntity(Cache_1.CacheCore.createObjectFromJSON(JSON.stringify(it))));
                    }
                    else if (Entity == '_UserEntity') {
                        this.nodes = cacheObjects.map(it => new UserEntity_1.UserEntity(Cache_1.CacheCore.createObjectFromJSON(JSON.stringify(it))));
                    }
                }
                else {
                    const objects = (yield TaskQueue_1.doTask(() => {
                        const results = query.find();
                        return results;
                    }));
                    this.nodes = objects.map(it => new (this.clazz)(it)).filter(it => {
                        if (it.verify instanceof Function) {
                            return it.verify();
                        }
                        return true;
                    });
                    //flush()
                    console.log("useQuery1");
                    if (query.useCache) {
                        //console.log(objects)
                        //console.log(query.className)
                        set(cacheKey, JSON.stringify(objects.map(it => it.toFullJSON())));
                    }
                }
            }
            else {
                const objects = (yield TaskQueue_1.doTask(() => {
                    const results = query.find();
                    return results;
                }));
                this.nodes = objects.map(it => new (this.clazz)(it)).filter(it => {
                    if (it.verify instanceof Function) {
                        return it.verify();
                    }
                    return true;
                });
                //flush()
                console.log("useQuery2");
            }
            //console.log(this)
            this.pageInfo = new common_1.PageInfo(this.nodes.length >= limit, skip + limit);
            return this;
        });
    }
    static cacheKey(query) {
        let cacheKey = query.className;
        if (JSON.stringify(query._select) !== "[]") {
            cacheKey += "_" + query._select.join("_");
        }
        if (JSON.stringify(query._include) !== "[]") {
            cacheKey += "_" + query._include.join("_");
        }
        if (JSON.stringify(query._where) !== "{}") {
            if (query._where.$relatedTo && query._where.$relatedTo.object) {
                //console.log(query._where.$relatedTo.object)
                // console.log(query._where.$relatedTo.object.__type)
                // console.log(query._where.$relatedTo.object.className)
                // console.log(query._where.$relatedTo.object.objectId)
                if (query._where.$relatedTo.object.__type === "Pointer") {
                    cacheKey += "_" + "Pointer_to_"
                        + query._where.$relatedTo.object.className + "_"
                        + "id_" + query._where.$relatedTo.object.objectId;
                }
            }
            else {
                cacheKey += JSON.stringify(query._where);
            }
        }
        cacheKey += "_" + query._skip + "_" + query._limit;
        //console.log(query)
        return cacheKey;
    }
}
exports.Connection = Connection;
class TodoConnection extends Connection {
    constructor() {
        super(...arguments);
        this.clazz = TodoEntity_1.TodoEntity;
    }
    static buildQuery(q = undefined) {
        const query = q || new leancloud_storage_1.Query("Todo");
        return query;
    }
}
exports.TodoConnection = TodoConnection;
class UserConnection extends Connection {
    constructor() {
        super(...arguments);
        this.clazz = UserEntity_1.UserEntity;
    }
    static buildQuery(q = undefined) {
        const query = q || new leancloud_storage_1.Query("_User");
        return query;
    }
}
exports.UserConnection = UserConnection;
