"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Connections_1 = require("./Connections");
class TodoEntity {
    constructor(obj) {
        this.obj = obj;
        //console.log(obj)
    }
    get id() {
        return this.obj.id;
    }
    get name() {
        return this.obj.get("name");
    }
    get type() {
        return this.obj.get("type");
    }
    get student() {
        //const query = UserConnection.buildQuery(this.obj.relation("student").query())
        const query = Connections_1.UserConnection.buildQuery(this.obj.relation("student").query());
        query.cache();
        //console.log(this.obj.relation("student").query().find())
        //console.log(query)
        //console.log(this.obj.relation("student").query())
        //console.log(query)
        return new Connections_1.UserConnection().exec(query, 0, 10);
    }
}
exports.TodoEntity = TodoEntity;
