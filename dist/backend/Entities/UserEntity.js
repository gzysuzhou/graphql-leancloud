"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserEntity {
    constructor(obj) {
        this.obj = obj;
    }
    get id() {
        return this.obj.id;
    }
    get mobilePhone() {
        return this.obj.get("moblePhone");
    }
    get username() {
        return this.obj.get("username");
    }
}
exports.UserEntity = UserEntity;
