"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PageInfo {
    constructor(hasNextPage, nextCursor) {
        this.hasNextPage = hasNextPage;
        this.nextCursor = nextCursor;
        if (!hasNextPage) {
            this.nextCursor = undefined;
        }
    }
}
exports.PageInfo = PageInfo;
