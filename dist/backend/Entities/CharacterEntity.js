"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class CharacterEntity {
    constructor(obj) {
        this.obj = obj;
    }
    get id() {
        return this.obj.id;
    }
    get name() {
        return this.obj.get('name');
    }
    avatar() {
        if (this.obj.get('avatar')) {
            return this.obj.get('avatar').thumbnailURL(120, 120);
        }
        return undefined;
    }
    largeAvatar() {
        if (this.obj.get('largeAvatar')) {
            return this.obj.get('largeAvatar').url();
        }
        return undefined;
    }
    get birthDate() {
        return this.obj.get('birthDate');
    }
    properties() {
        return __awaiter(this, void 0, void 0, function* () {
            return JSON.stringify(this.obj.get('properties') || {});
        });
    }
    wishPoint() {
        return __awaiter(this, void 0, void 0, function* () {
            return Math.round(this.obj.get('wishPoint'));
        });
    }
    summary() {
        return this.obj.get('summary');
    }
}
exports.CharacterEntity = CharacterEntity;
